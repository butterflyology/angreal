"""
    angreal
    ~~~~~~~


"""
from angreal.utils import get_angreal_path, import_from_file
from angreal.decorators import *
import importlib
import os

importlib.import_module('click')

__all__ = [
    # Click Decorators
    'pass_context', 'pass_obj', 'make_pass_decorator', 'command', 'group',
    'argument', 'option', 'confirmation_option', 'password_option',
    'version_option', 'help_option',

    # Click Utilities
    'echo', 'get_binary_stream', 'get_text_stream', 'open_file',
    'format_filename', 'get_app_dir', 'get_os_args',

]

__version__ = open( os.path.join( os.path.dirname(__file__), 'VERSION')).read().strip()