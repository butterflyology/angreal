============
Contributing
============

Angreal is hosted on `gitlab <https://gitlab.com/dylanbstorey/angreal>`_.

If you have questions, concerns, bug reports, or suggestions please feel free to open an
`issue <https://gitlab.com/dylanbstorey/angreal/issues/new>`_, I'll do my best to get it addressed at any time.


If you'd like to contribute back to angreal's code base (or documentation!) feel free to submit a
`merge request <https://gitlab.com/dylanbstorey/angreal/merge_requests/new>`_.

Before submitting a merge request, it would be best if you open a new issue that outlines what the problem you wish to
solve is (and perhaps see if anyone else is working on it).



**Setting up for development:**


- Clone the source code : ``git clone git@gitlab.com:dylanbstorey/angreal.git``
- Navigate to angreal : ``cd angreal``
- Set up an environment : ``conda create -n angreal python=3``
- Activate environment : ``source activate angreal``
- Install dependencies : ``pip install -r requirements/dev.txt``
- Go have fun !



