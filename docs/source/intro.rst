#############
Introduction
#############



.. admonition:: TL;DR

    **Angreal is meant to :**

    - allow the consistent setup of projects
    - provide consistent methods for interacting with the project

Why?
====

Angreal is an attempt to solve two problems that i was running into in both my personal and professional life as a data
scientist and software developer. I do things often enough that they needed automation, I don't do things so often that I
remember all of the steps/commands I might need to get them done. Angreal solves this problem by allowing me to remember
by forgetting : I only have to remember the command to do something not the actual steps to complete the task.




So many tools so little time ...
================================

I think it's fair to say that when writing and documenting software individuals spend at least as much
time working on the process of creating, verifying, and distributing their work as they do creating new content.

**Angreal allows common processes needed within a project (testing, linting, creation, deployment,documentation, etc.) to travel with the project. Meaning that not only the project,
but the way people interact with it is consistent.**



