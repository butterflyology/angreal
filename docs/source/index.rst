.. include:: ../../README.rst

.. include:: intro.rst

.. toctree::
   :maxdepth: 2

   quickstart
   tutorials
   how-to-guides
   reference
   discussion
   contributing
   code_of_conduct

